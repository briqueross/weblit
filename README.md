Sample project to show how Lition could deploy a participatory translation program for its website.

Translations could be handled on Crowdin : https://crowdin.com/project/lition-translation-example

Then, JSON translation files are commited into Git.
This makes it easier to use translations in project.